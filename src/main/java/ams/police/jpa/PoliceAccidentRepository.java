package ams.police.jpa;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PoliceAccidentRepository extends CrudRepository<PoliceAccident, Long> {
}
