package ams.police.service;

import ams.police.jpa.PoliceAccident;
import ams.police.jpa.PoliceAccidentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PoliceAccidentService {
    private final PoliceAccidentRepository policeAccidentRepository;

    @Transactional(rollbackFor = Exception.class)
    public void saveBatch(List<PoliceAccident> batch){
        policeAccidentRepository.saveAll(batch);
    }
}
