package ams.police.mapper;

import ams.data.model.PoliceEventModel;
import ams.police.jpa.PoliceAccident;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PoliceMapper {
    PoliceMapper MAPPER = Mappers.getMapper(PoliceMapper.class);
    @Mapping(source = "address", target = "address", qualifiedByName = "charSequenceToString")
    @Mapping(source = "latitude", target = "latitude", qualifiedByName = "charSequenceToString")
    @Mapping(source = "longitude", target = "longitude", qualifiedByName = "charSequenceToString")
    @Mapping(source = "description", target = "description", qualifiedByName = "charSequenceToString")
    PoliceAccident policeEventModelToPoliceAccident(PoliceEventModel source);

    @Named("charSequenceToString")
    default String charSequenceToString(CharSequence charSequence) {
        return charSequence != null ? charSequence.toString() : null;
    }
}
