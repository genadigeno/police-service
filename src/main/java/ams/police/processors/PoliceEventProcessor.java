package ams.police.processors;

import ams.data.model.PoliceEventModel;
import ams.police.jpa.PoliceAccident;
import ams.police.jpa.PoliceAccidentRepository;
import ams.police.mapper.PoliceMapper;
import ams.police.service.PoliceAccidentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class PoliceEventProcessor {
    private final PoliceAccidentService policeAccidentService;

    public void process(List<ConsumerRecord<String, PoliceEventModel>> records) {
        log.info("total messages - {}", records.size());

        List<PoliceAccident> batch = new ArrayList<>();
        try {
            for (ConsumerRecord<String, PoliceEventModel> rec : records){
                log.info("Record: value - {}, key - {}", rec.value(), rec.key());
                //collect in a batch
                batch.add(PoliceMapper.MAPPER.policeEventModelToPoliceAccident(rec.value()));
            }
        } catch (Exception e) {
            log.error("Error processing record", e);
            throw e;//re-throw an exception to trigger the recoverer
        } finally {
            log.info("batch size - {}", batch.size());
            policeAccidentService.saveBatch(batch);//in case an exception still save a batch we got before the exception
            log.info("batch saved");
        }

    }
}
